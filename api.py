#!/usr/bin/python

from flask import Flask, json, request
import os

api = Flask(__name__)

@api.route('/')
def index():
    """
        method index

        get index page
    """
    return "Index page"

if __name__ == '__main__':
    port = int(os.environ.get('PORT', 33507)) 
    api.run(host='0.0.0.0', port=port)
