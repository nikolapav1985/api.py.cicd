#!/usr/bin/python

import unittest
import api as myapi
import json
import sys

class TestFlaskApi(unittest.TestCase):
    def setUp(self):
        self.app = myapi.api.test_client()

    def test_hello_world(self):
        response = self.app.get('/')
        self.assertEqual(
            response.status_code,
            200
        )

if __name__ == '__main__':
    unittest.main()

