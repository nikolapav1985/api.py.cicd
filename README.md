Description
-----------

CI / CD example, set up a pipeline, test and deploy.

Details
-------

- Couple of stages (test and deploy).
- Stack (GitLab, Heroku, Python).
- More available in .gitlab-ci.yml.

Example
-------

- Check [here](https://cicdstage.herokuapp.com/) (running application example).
